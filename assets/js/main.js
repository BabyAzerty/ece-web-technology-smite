$(document).ready(function(){
	//On utilise notify pour les messages d'erreurs ou de succes
	if($("#secret_success").text() != ""){
		var noty_message = noty({
									type: 'success',
									text: $("#secret_success").text()
								});
	}
	if($("#secret_error").text() != ""){
		var noty_error = noty({
									type: 'error',
									text: $("#secret_error").text()
								});
	}
	if($("#secret_message").text() != ""){
		var noty_error = noty({
									type: 'information',
									text: $("#secret_message").text()
								});
	}
	//On utilise le tooltip jQuery UI sur l'ensemble de la page
	$(document).tooltip();
});