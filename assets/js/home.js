/**
 * @author Jerome Pasquier
 *	Système de Prototypage
 */
 //Variable utilisée dans le html
var thisHome;

//Prototypage
function Home(){
}

//Fonction init lors du document ready
Home.prototype.init = function(){
	
}

//Fonction de validation du formulaire d'inscription
Home.prototype.validateForm = function(){
	var hasError = 0;
	if($("#formPseudo").val()==""){
		hasError = 1;
		$("#formPseudo-erreur").show();
	} else {
		$("#formPseudo-erreur").hide();
	}
	if($("#formEmail").val()==""){
		hasError = 1;
		$("#formEmail-erreur").show();
	} else {
		if(!validateEmail($("#formEmail").val())){
			hasError = 1;
			$("#formEmail-erreur").show();
		} else {
			$("#formEmail-erreur").hide();
		}
	}
	if($("#formPassword").val()==""){
		hasError = 1;
		$("#formPassword-erreur").show();
	} else {
		$("#formPassword-erreur").hide();
	}
	//If everything is good, we can send new password
	if(!hasError){
		$("#form").submit();
	}
}

//Simple function de vérification d'email via regexp
function validateEmail(email) { 
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(email);
}

//Lorsque le DOM est chargé
$(document).ready(function(){
	thisHome = new Home;
	thisHome.init();
});