/**
 * @author Jerome Pasquier
 *	Système de Prototypage
 */
 //Variable utilisée dans le html
var thisMember;

//Prototypage
function Member(){
}

//Fonction init lors du document ready
Member.prototype.init = function(){
	$( "#dialogCommentaire" ).dialog({
        autoOpen: false,
        width: 'auto',
        height:'auto',
        title: 'Poser un commentaire',
        buttons: {
                "Envoyer le commentaire": function(){
                		$.post('addCommentaire',
                				{commentaire:$("#formCommentaireTextarea").val(),
                				logo:$("#formCommentaireLogo").val(),
                				idFrom:$("#formCommentaireIdFrom").val(),
                				idTo:$("#formCommentaireIdTo").val()
                				},
		                		function(data) {
									alert(data);
									$.get('ajaxGetComments',{id:$("#formCommentaireIdTo").val()},
									function(data){
										$("#tableComments").html(data);
									});
						});
                        $( this ).dialog( "close" );
                        },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    	}
                 },
        close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
        }
    });
    $( "#dialogStatut" ).dialog({
        autoOpen: false,
        width: 'auto',
        height:'auto',
        title: 'Changer de statut',
        buttons: {
                "Changer le statut": function(){
                		$.post('updateStatut',
                				{statut:$("#formStatutTextarea").val(),
                				id:$("#formStatutId").val()
                				},
		                		function(data) {
									alert(data);
									$.get('ajaxGetStatut',{id:$("#formStatutId").val()},
									function(data){
										$("#tableStatut").html(data);
									});
						});
                        $( this ).dialog( "close" );
                        },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    	}
                 },
        close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
        }
    });
}

//Fonction d'ouverture du dialog box de commentaires
Member.prototype.showDialogForComment = function(){
	$("#dialogCommentaire").dialog("open");
}

//Fonction d'ouverture du dialog box de statut
Member.prototype.showDialogForStatut = function(){
	$("#dialogStatut").dialog("open");
}

//Lorsque le DOM est chargé
$(document).ready(function(){
	thisMember = new Member;
	thisMember.init();
});