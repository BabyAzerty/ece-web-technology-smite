<div id="main">
	<div id="main-content">
		<div>
			Coucou !<br />
			Tout bon <span class="gold">dieu</span> devrait connaitre le temps qu'il fait. D'où l'utilisation d'une API de <span class="gold">Météo</span> (celle de <a href="http://www.wunderground.com/">Wunderground Weather</a>) !<br />
			<span class="small">Cette API se limite à 10 requête par minute</span><br /><br />
			Voici la météo pour le prochain jour<br />
			<div>
			<?php
				echo "<span class='gold'>Lieu</span> : ".$json->forecast->simpleforecast->forecastday[0]->date->tz_long."<br />";
				echo "<span class='gold'>Météo pour</span> : ".$json->forecast->txt_forecast->forecastday[0]->title."<br />";
				echo "<span class='gold'>Il fera</span> : "."<img title='".$json->forecast->txt_forecast->forecastday[0]->icon."' src='".$json->forecast->simpleforecast->forecastday[0]->icon_url."' />".$json->forecast->txt_forecast->forecastday[0]->fcttext_metric."<br />";
			?>
			</div>
		</div>
	</div>
</div>