		<div id="footer">
			<ul id="footer-menu">
				<li><a href="<?php echo site_url();?>/weather">API</a></li>
				<li><a href="<?php echo site_url();?>/member<?php if($this->session->userdata('logged_in')){ echo "/showProfile?id=".$this->session->userdata('id');}else{ echo "/showProfile";}?>">Profile</a></li>
				<li><a href="<?php echo site_url();?>/member">Members</a></li>
				<li><a href="http://www.twitter.com">Twitter</a></li>
				<li><a href="<?php echo site_url();?>/member/showRSS">RSS</a></li>
				<?php if($this->session->userdata('logged_in')): ?>
					<li><a href="<?php echo site_url();?>/member/logout">Log Out</a></li>
				<?php else: ?>
					<li><a href="<?php echo site_url();?>/member/signIn">Sign In</a></li>
				<?php endif; ?>
			</ul>
		</div>
	</body>
</html>