<?php
	$motto = array("Gank !", "Attack !", "Defend !", "Retreat !", "Help !");
?>
<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" href="<?php echo base_url();?>assets/css/jqueryui.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/jqueryui.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/noty/jquerynoty.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/noty/layouts/top.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/noty/themes/default.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/main.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php
		//If custom metas available, insert them here
		if(isset($meta)){
			foreach ($meta as $key => $value) {
				echo "<meta name=\"".$key."\" content=\"".$value."\" />";
			}
		}
		
		//If custom css files needed, insert them here
		if(isset($css)){
			foreach ($css as $key => $value) {
				echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"".base_url()."assets/css/".$value."\" />";
			}
		}
		
		//If custom js files needed, insert them here
		if(isset($js)){
			foreach ($js as $key => $value) {
				echo "<script type=\"text/javascript\" src=\"".base_url()."assets/js/".$value."\"></script>";
			}
		}
		
		//If custom title, insert it here ELSE All4Team title by default
		if(isset($title)){
			echo "<title>".$title."</title>";
		} else {
			echo "<title>Smite</title>";
		}
		?>
	</head>
	<body>
		<div id="header">
			<div id="header-inner">
				<p id="header-img"><a href="<?php echo site_url();?>/home"><img id="banner" height="260" alt="logo smite" src="<?php echo base_url();?>assets/img/logo_color.png"/></a></p>
				<h2 id="header-motto"><?php if($this->session->userdata('logged_in')){echo "<a class='small'>Logged as ".$this->session->userdata('username')."</a>";}else{echo $motto[rand(0, count($motto)-1)];} ?></h2>
			</div>
		</div>
		<div id="secret_success" class="hide"><?php if(!empty($message_success)) echo $message_success;?></div>
		<div id="secret_error" class="hide"><?php if(!empty($message_error)) echo $message_error;?></div>
		<div id="secret_message" class="hide"><?php if(!empty($message_message)) echo $message_message;?></div>