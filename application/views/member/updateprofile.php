<div id="main">
	<div id="main-content">
		<p>ID Card of <span class="gold">GOD</span> :</p>
		<a class="error">
		<?php echo $message_error; ?>
		</a>
		<a class="success">
		<?php echo $message_success; ?>
		</a>
		<?php foreach ($member as $info) { ?>
		<form method="post" action="<?php echo site_url();?>/member/updateProfile">
			<fieldset>
				<legend>Modification de l'ID Card</legend>
				<label for="formPseudo">Pseudo :</label><input id="formPseudo" type="text" name="pseudo" value="<?php echo $info['member_pseudo'];?>"/><br />
				<label for="formDescription">Description :</label><input id="formDescription" type="text" name="description" value="<?php echo $info['member_description'];?>"/><br />
				<label for="formEmail">Email :</label><input id="formEmail" type="text" name="email" value="<?php echo $info['member_email'];?>"/><br />
				<label for="formUrl">Url :</label><input id="formUrl" type="text" name="url" value="<?php echo $info['member_url'];?>"/><br />
				<label for="formPassword">Password :</label><input id="formPassword" type="password" name="password"/><br />
				<select id="formGender" name="sex">
					<option value="male" <?php if($info['member_sex']=="male") echo "selected='selected'"; ?>>Male</option>
					<option value="female" <?php if($info['member_sex']=="female") echo "selected='selected'"; ?>>Female</option>
				</select><br />
				<input type="hidden" value="<?php echo $info['member_id'];?>" name="id"/>
				<input type="hidden" value="UPDATE_MEMBER" name="ACTION"/>
				<input type="submit" value="Submit" id="formSubmit"/><br />
			</fieldset>
		</form>
		<p id="buttonDelete"><a href="<?php echo site_url(); ?>/member/deleteProfile?id=<?php echo $info['member_id']; ?>"><input type="button" value="Delete DEFINITELY" /></p>
		<?php } ?>
	</div>
</div>