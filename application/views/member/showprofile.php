<div id="main">
	<div id="main-content">
		<p>ID Card of <span class="gold">GOD</span> :</p>
		<table class="leftSmallTable">
			<?php
				foreach ($member as $info) {
					echo '<tr>';
					echo '<td>';
					echo '<p class="gold">'.$info['member_pseudo'].'</p>';
					echo '<p>'.$info['member_description'].'</p>';
					echo '<p>'.$info['member_email'].'</p>';
					echo '<p><a href="http://'.$info['member_url'].'">'.$info['member_url'].'</a></p>';
					echo '<p><a href="'.site_url().'/member/showProfile?id='.$info['member_id'].'"><img alt="logo" class="main-imgRounded" src="'.base_url().'assets/img/Icone/'.$info["member_imgPath"].'"/></a></p>';
					echo '<p>Gender : '.$info['member_sex'].'</p>';
					//If logged, we can update/delete the user
					if($this->session->userdata('isAdmin')){
						echo '<p><a href="'.site_url().'/member/updateProfile?id='.$info['member_id'].'"><input type="button" value="Update/Delete this God" /></p>';
					}
					echo '</td>';
					echo '</tr>';
				}
			?>
			<?php
			if($this->session->userdata('logged_in')):
				//On affiche le bouton commenter que si le profile visité n'est pas celui de l'utilisateur connecté
				if($this->session->userdata('id') != $member[0]['member_id']):
			?>
			<tr>
				<td><button type="button" class="bigbutton2" onclick="javascript:thisMember.showDialogForComment();">Commenter</button></td>
			</tr>
			<?php
				elseif($this->session->userdata('id') == $member[0]['member_id']):
			?>
			<tr>
				<td><button type="button" class="bigbutton2" onclick="javascript:thisMember.showDialogForStatut();">Statut</button></td>
			</tr>
			<?php
				endif;
			endif;
			?>
		</table>
		<table id="tableComments" class="leftTable">
			<tr><td><a class="gold">Commentaires</a><br/><br/><td></tr>
			<?php
				foreach ($comments as $comment) {
					echo '<tr>';
					echo '<td><img class="left" alt="logo" src="'.base_url().'assets/img/Services/Color/'.$comment["comm_logo"].'"/>De : <span class="gold">'.$comment['member_pseudo'].'</span><br/>';
					echo 'À : '.date('r',$comment['comm_date']).'<br/>';
					echo $comment['comm_content'];
					echo '<br/><br/>';
					echo '</td>';
					echo '</tr>';
				}
				if(count($comments) == 0){
					echo '<tr>';
					echo '<td>';
					echo 'Pas de commentaires ...';
					echo '</td>';
					echo '</tr>';
				}
			?>
		</table>
		<table id="tableStatut" class="leftTable">
			<tr><td><a class="gold">Statut</a><br/><br/><td></tr>
			<?php
				foreach($statut as $info)
				{
					echo '<tr>';
					echo '<td>';
					echo 'À : '.date('r',$info['statut_date']).'<br/>';
					echo $info['statut_content'];
					echo '<br/><br/>';
					echo '</td>';
					echo '</tr>';
				}
				if(count($statut) == 0){
					echo '<tr>';
					echo '<td>';
					echo 'Pas de statut ...';
					echo '</td>';
					echo '</tr>';
				}
			?>
		</table>
		<div class="clear"></div>
		<div id="dialogCommentaire">
			<form class="autoForm">
		        <textarea name="formCommentaireTextarea" id="formCommentaireTextarea" class="text ui-widget-content ui-corner-all" rows="4" cols="58"></textarea>
		        <br/>
		        <br/>
		        <label for="formCommentaireLogo">Choisir un logo qui accompagnera le commentaire</label>
		        <br/>
		        <br/>
		        <select id="formCommentaireLogo" name="formCommentaireLogo">
		        	<option value="facebook.png">Facebook</option>
		        	<option value="linkedin.png">LinkedIn</option>
		        	<option value="rss.png">RSS</option>
		        	<option value="twitter.png">Twitter</option>
		        	<option value="youtube.png">YouTube</option>
		        </select>
		        <input name="formCommentaireIdFrom" id="formCommentaireIdFrom" type="hidden" value="<?php echo $this->session->userdata('id');?>"/>
		        <input name="formCommentaireIdTo" id="formCommentaireIdTo" type="hidden" value="<?php echo $member[0]['member_id'];?>"/>
		    </form>
		</div>
		<div id="dialogStatut">
			<form class="autoForm">
		        <textarea name="formStatutTextarea" id="formStatutTextarea" class="text ui-widget-content ui-corner-all" rows="4" cols="58"></textarea>
		        <br/>
		        <br/>
		        <input name="formStatutId" id="formStatutId" type="hidden" value="<?php echo $this->session->userdata('id');?>"/>
		    </form>
		</div>
	</div>
</div>