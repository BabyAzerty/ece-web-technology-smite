<table id="tableComments" class="leftTable">
	<tr><td><a class="gold">Commentaires</a><br/><br/><td></tr>
	<?php
		foreach ($comments as $comment) {
			echo '<tr>';
			echo '<td><img class="left" alt="logo" src="'.base_url().'assets/img/Services/Color/'.$comment["comm_logo"].'"/>De : <span class="gold">'.$comment['member_pseudo'].'</span><br/>';
			echo 'À : '.date('r',$comment['comm_date']).'<br/>';
			echo $comment['comm_content'];
			echo '<br/><br/>';
			echo '</td>';
			echo '</tr>';
		}
		if(count($comments) == 0){
			echo '<tr>';
			echo '<td>';
			echo 'Pas de commentaires ...';
			echo '</td>';
			echo '</tr>';
		}
	?>
</table>