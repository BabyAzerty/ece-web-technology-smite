<div id="main">
	<div id="main-content">
		<p id="main-presentation">
			<span class="gold">SMITE</span> est un excellent jeu vidéo basé sur le concept de DOTA.<br />
			Mais ça, on s'en fout.<br />
			Ce site utilise la technologie <span class="css3gold">CSS3</span> et <span class="css3gold">HTML5</span>.<br />
			<ul>
				<li>Les @font-face</li>
				<li>Les Border Radius</li>
				<li>Les Text Shadow</li>
				<li>Les Box Shadow</li>
				<li>Les Gradients</li>
			</ul>
			Ainsi que les technologies suivantes : <br />
			<ul>
				<li>AJAX lors d'un post de commentaire ou de statut</li>
				<li>Les tooltips (sur le formulaire ci-dessous)</li>
				<li>Les boites de dialog pour le post des commentaires/statut</li>
				<li>Une animation Javascript lors d'une erreur de login</li>
			</ul>
		</p>
		<a class="error">
		<?php echo $message_error; ?>
		</a>
		<a class="success">
		<?php echo $message_success; ?>
		</a>
		<form method="post" action="<?php echo site_url();?>/member/signUp" id="form">
			<fieldset>
				<legend>Inscription au Panthéon</legend>
				<label for="formPseudo">Pseudo*:</label><input id="formPseudo" type="text" name="pseudo" title="Obligatoire"/><a id="formPseudo-erreur" class="error hide">Erreur</a><br />
				<label for="formDescription">Description :</label><input id="formDescription" type="text" name="description" title="Vos exploits divins ?"/><br />
				<label for="formEmail">Email*:</label><input id="formEmail" type="text" name="email" title="Obligatoire"/><a id="formEmail-erreur" class="error hide">Erreur</a><br />
				<label for="formPassword">Password*:</label><input id="formPassword" type="password" name="password" title="Obligatoire"/><a id="formPassword-erreur" class="error hide">Erreur</a><br />
				<select id="formGender" name="sex">
					<option value="male">Male</option>
					<option value="female">Female</option>
				</select><br />
				<input type="button" value="Submit" id="formSubmit" onclick="javascript:thisHome.validateForm();"/><br />
			</fieldset>
		</form>
		<div class="clear"></div>
		<table id="main-tableProfiles">
			<tr>
				<?php
					foreach ($frontProfiles as $profile) {
						echo '<td rowspan="2" class="main-tableProfiles-img"><a href="'.site_url().'/member/showProfile?id='.$profile['member_id'].'"><img alt="logo" class="main-imgRounded" src="'.base_url().'assets/img/Icone/'.$profile["member_imgPath"].'"/></a></td>';
						echo '<td class="main-tableProfiles-name"><a href="'.site_url().'/member/showProfile?id='.$profile['member_id'].'">'.$profile["member_pseudo"].'</a></td>';
					}
				?>
			</tr>
			<tr>
				<?php
					foreach ($frontProfiles as $profile) {
						echo '<td class="main-tableProfiles-name"><a href="'.site_url().'/member/showProfile?id='.$profile['member_id'].'">'.$profile["member_description"].'</a></td>';
					}
				?>
			</tr>
		</table>
	</div>
</div>