<?php
    class Profile_model extends CI_Model
    {
    	public $_firstname;
		public $_lastname;
		public $_email;
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getComments($nbrOfComments = 0)
		{
			$this->db->order_by("comm_id","random");
			
			//If nbrOfComments is empty, it means show all gods else only show what's asked
			if($nbrOfComments != 0)
				$this->db->limit($nbrOfComments);
				
			$query = $this->db->get("Smite_comment");
			
			return $query->result_array();
		}
    }
?>