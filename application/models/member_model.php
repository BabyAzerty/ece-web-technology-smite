<?php
	class Member_model extends CI_Model
	{
		public $_pseudo;
		public $_email;
		public $_password;
		public $_gender;
		
		public function __construct()
		{
			//$this->_pseudo = $username;
			parent::__construct();
		}
		
		public function addMember($post)
		{
			$data = array(
				'member_pseudo'		=> $post['pseudo'],
				'member_description'=> $post['description'],
				'member_password'	=> $post['password'],
				'member_email' 		=> $post['email'],
				'member_sex' 		=> $post['sex'],
				'member_imgPath'	=> "user.png",
				'member_isAdmin'	=> 0
			);
			
			$this->db->insert('Smite_member', $data);
		}
		
		public function updateMember($post)
		{
			$data;
			//If no password, then do not change it
			if($post['password'] == ""){
				$data = array(
					'member_pseudo'		=> $post['pseudo'],
					'member_description'=> $post['description'],
					'member_email' 		=> $post['email'],
					'member_sex' 		=> $post['sex'],
					'member_url'		=> $post['url']
				);
			} else {
				$data = array(
					'member_pseudo'		=> $post['pseudo'],
					'member_description'=> $post['description'],
					'member_password'	=> $post['password'],
					'member_email' 		=> $post['email'],
					'member_sex' 		=> $post['sex'],
					'member_url'		=> $post['url']
				);
			}
			
			$this->db->where('member_id', $post['id']);
			$this->db->update('Smite_member',$data);
		}
		
		public function deleteProfileById($id)
		{
			$this->db->where('member_id', $id);
			$this->db->delete('Smite_member'); 
		}
		
		//////////////////////////////////////////////////////////
		// LOGIN && ADMIN										//
		//	1.Login with pseudo & password						//
		//	2.Return FALSE if not found or isAdmin info if found//
		//////////////////////////////////////////////////////////
		public function login($post)
		{
			$this->db->select('member_isAdmin, member_id');
			$query = $this->db->get_where('Smite_member', array('member_pseudo' => $post['pseudo'], 'member_password' => $post['password']));
			if($query->num_rows()){
				return $query->result_array();
			} else {
				return NULL;
			}
		}
		
		public function getRSS()
		{
			$this->db->order_by("member_id","desc");
			$this->db->limit(10);
			$query = $this->db->get("Smite_member");
			return $query->result_array();
		}
		
		public function getFrontProfiles($nbrOfProfile = 0)
		{	
			$this->db->select("member_id, member_pseudo, member_description, member_imgPath, member_email");
			$this->db->order_by("member_pseudo","random");
			
			//If nbrOfProfile is empty, it means show all gods
			if($nbrOfProfile != 0)
				$this->db->limit($nbrOfProfile);
			$query = $this->db->get("Smite_member");
			
			return $query->result_array();
		}
		
		public function getProfileById($id)
		{
			$query = $this->db->get_where('Smite_member', array('member_id' => $id));
			return $query->result_array();
		}
		
		public function getStatutById($id)
		{
			$query = $this->db->get_where('Smite_statut', array('statut_from' => $id));
			return $query->result_array();
		}
		
		public function getCommentsToId($id)
		{
			$query = $this->db->query("SELECT * FROM Smite_comment comm JOIN Smite_member mem ON comm.comm_from = mem.member_id WHERE comm_to = $id");
			return $query->result_array();
		}
		
		public function addCommentaire($post)
		{
			$data = array(
						'comm_from'		=> $post['idFrom'],
						'comm_to'		=> $post['idTo'],
						'comm_content'	=> $post['commentaire'],
						'comm_logo'		=> $post['logo'],
						'comm_date'		=> time()
						);
			$this->db->insert('Smite_comment',$data);
		}
		
		public function changeStatut($post)
		{
			$id = $post['id'];
			$query = $this->db->query("SELECT * FROM Smite_statut WHERE statut_from = $id");
			
			if($query->num_rows())
			{
				$data = array(
				               'statut_content' => $post['statut']
				            );
				
				$this->db->where('statut_from', $post['id']);
				$this->db->update('Smite_statut', $data); 
			} else {
				$data = array(
							'statut_from'	=> $post['id'],
							'statut_content'=> $post['statut'],
							'statut_date'	=> time()
							);
				$this->db->insert('Smite_statut',$data);	
			}
		}
	}
?>