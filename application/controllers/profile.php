<?php
    class Profile extends CI_Controller
    {	
		public function index()
		{
			$this->load->helper('date');
			$this->load->model("profile_model","profdb");
			$this->data['comments'] = $this->profdb->getComments(3);
			
			$this->data['title'] = "Smite Profile";
			
			$this->load->view('common/header.php',$this->data);
			$this->load->view('profile/index.php', $this->data);
			$this->load->view('common/footer.php');
		}
    }
?>