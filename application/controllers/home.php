<?php
    class Home extends CI_Controller
    {
		public function index()
		{
			$this->data['js']['home'] = 'home.js';
			$this->load->model('member_model','memdb');
			
			$this->data['frontProfiles'] = $this->memdb->getFrontProfiles(3);
			
			$this->data['title'] = "Smite Home";
			
			$this->data['message_error'] = $this->session->flashdata('message_error');
			$this->data['message_success'] = $this->session->flashdata('message_success');
			$this->load->view('common/header.php',$this->data);
			$this->load->view('home/home.php', $this->data);
			$this->load->view('common/footer.php');
		}
    }
?>