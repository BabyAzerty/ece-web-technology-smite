<?php
    class Weather extends CI_Controller
    {	
		public function index()
		{
			$this->data['title'] = "Smite Weather";
			$this->data['json'] = json_decode(file_get_contents('http://api.wunderground.com/api/76d18f1195bbc23e/forecast/lang:FR/q/FR/Paris.json'));
			$this->load->view('common/header.php',$this->data);
			$this->load->view('weather/index.php', $this->data);
			$this->load->view('common/footer.php');
		}
    }
?>