<?php
    class Member extends CI_Controller
    {
    	public function __construct()
    	{
	    	parent::__Construct();
	    	$this->data['js']['member'] = 'member.js';
    	}
    	//Fonction d'affichage de la liste des membres
		public function index()
		{
			$this->load->model('member_model','memdb');
			
			$this->data['members'] = $this->memdb->getFrontProfiles();
			$this->data['title'] = "Smite Gods";
			
			$this->load->view('common/header.php',$this->data);
			$this->load->view('member/index.php', $this->data);
			$this->load->view('common/footer.php');
		}
		
		public function showRSS()
		{
			$this->load->helper('xml');
			$this->load->model('member_model','memdb');
			
			$this->data['rss'] = $this->memdb->getRSS();

			$this->data['currentDate'] = date('r');
			$this->data['encoding'] = 'utf-8';
	        $this->data['feed_name'] = 'smite.com';
	        $this->data['feed_url'] = 'http://www.smite.com';
	        $this->data['page_description'] = 'Last 10 Smite members created';
	        $this->data['page_language'] = 'fr';
	        $this->data['creator_email'] = '';
	        header("Content-Type: application/rss+xml");
			$this->load->view('member/showrss.php', $this->data);
		}
		
		//Fonction d'affichage des info de membre
		public function showProfile()
		{
			if(!$this->input->get('id',TRUE)){
				$this->session->set_flashdata('message_message','Veuillez vous connecter avant de consulter votre page de profil');
				redirect("member/signIn","refresh");
			}
			$this->load->model('member_model','memdb');
			
			$this->data['member'] = $this->memdb->getProfileById($this->input->get('id',TRUE));
			$this->data['comments'] = $this->memdb->getCommentsToId($this->input->get('id', TRUE));
			$this->data['statut'] = $this->memdb->getStatutById($this->input->get('id', TRUE));
			$this->data['title'] = "Show Member";
			
			$this->load->view('common/header.php',$this->data);
			$this->load->view('member/showprofile.php', $this->data);
			$this->load->view('common/footer.php');
		}
		
		//Fonction de mise à jour des info de membre
		public function updateProfile()
		{
			$this->load->model('member_model','memdb');
			
			//If not logged in, cannot change
			if(!$this->session->userdata('isAdmin'))
			{
				redirect('member/showProfile?id='.$this->input->get('id',TRUE),'refresh');
			} else {
			
				if($this->input->post('ACTION',TRUE) == "UPDATE_MEMBER")
				{
					$error = "-";
					if($this->input->post('pseudo',TRUE) == ""){
						$error .= " Missing pseudo -";
					}
					
					if($this->input->post('description',TRUE) == ""){
						$error .= " Missing description -";
					}
					
					if($this->input->post('email',TRUE) == ""){
						$error .= " Missing email -";
					}
					
					//Si on oublie un champ, on retourne au formulaire avec un message d'erreur
					if($error != "-"){
						$this->session->set_flashdata('message_error', $error);
						redirect('member/updateProfile','refresh');
					} else {
						$this->memdb->updateMember($this->input->post(NULL, TRUE));
						$this->session->set_flashdata('message_success', "Modification de la license effective !");
						redirect('member/updateProfile','refresh');
					}
				}
			
				$this->data['member'] = $this->memdb->getProfileById($this->input->get('id',TRUE));
				$this->data['title'] = "Update Member";
				
				$this->data['message_error'] = $this->session->flashdata('message_error');
				$this->data['message_success'] = $this->session->flashdata('message_success');
			
				$this->load->view('common/header.php',$this->data);
				$this->load->view('member/updateprofile.php', $this->data);
				$this->load->view('common/footer.php');
			}
		}
		
		//Fonction de suppression de membre
		public function deleteProfile()
		{
			if($this->session->userdata('isAdmin'))
			{
				$this->load->model('member_model','memdb');
				
				$this->memdb->deleteProfileById($this->input->get('id',TRUE));
				$this->data['title'] = "Delete Member";
				
				$this->load->view('common/header.php',$this->data);
				$this->load->view('member/deleteprofile.php', $this->data);
				$this->load->view('common/footer.php');
			} else {
				redirect("member","refresh");
			}
		}
		
		//Fonction de connexion
		public function signIn()
		{
			//Si on essaie de login
			if($this->input->post('ACTION',TRUE) == "signIn")
			{
				$this->load->model('member_model','memdb');
				
				$result = $this->memdb->login($this->input->post(NULL, TRUE));
				
				if($result != NULL)
				{
					$this->session->set_flashdata('message_success',"Bienvenue à toi grand dieu !");
					$newdata = array(
					                   'username'	=> $this->input->post('pseudo'),
					                   'id'			=> $result[0]['member_id'],
					                   'logged_in'	=> TRUE,
					                   'isAdmin'	=> $result[0]['member_isAdmin']
					               );
					
					$this->session->set_userdata($newdata);
					redirect("member/showProfile?id=".$result[0]['member_id'],"refresh");
				} else {
					$this->session->set_flashdata('message_error',"Misérable mortel, l'Olympe est réservé aux vrais dieux (avec un vrai compte) !");
					redirect("member/signin","refresh");
				}
			}
			
			$this->data['title'] = "Member Sign In";
			
			$this->data['message_error'] = $this->session->flashdata('message_error');
			$this->data['message_success'] = $this->session->flashdata('message_success');
			$this->data['message_message'] = $this->session->flashdata('message_message');
			$this->load->view('common/header.php',$this->data);
			$this->load->view('member/signin.php', $this->data);
			$this->load->view('common/footer.php');
		}
		
		//Fonction de déconnexion
		public function logOut()
		{
			$this->session->sess_destroy();
			
			$this->data['title'] = "Log Out";
			
			$this->load->view('common/header.php',$this->data);
			$this->load->view('member/logout.php', $this->data);
			$this->load->view('common/footer.php');
		}
		
		//Fonction d'ajout de membre
		public function signUp()
		{
			$this->load->model('member_model','memdb');
			
			$error = "-";
			if($this->input->post('pseudo',TRUE) == ""){
				$error .= " Missing pseudo -";
			}
			
			if($this->input->post('description',TRUE) == ""){
				$error .= " Missing description -";
			}
			
			if($this->input->post('email',TRUE) == ""){
				$error .= " Missing email -";
			}
			
			if($this->input->post('password',TRUE) == ""){
				$error .= " Missing password -";
			}
			
			//Si on oublie un champ, on retourne au formulaire avec un message d'erreur
			if($error != "-"){
				$this->session->set_flashdata('message_error', $error);
				redirect('home','refresh');
			} else {
				$this->memdb->addMember($this->input->post(NULL, TRUE));
				$this->session->set_flashdata('message_success', "Héro ajouté au Panthéon !");
				redirect('home','refresh');
			}
		}
		
		//Fonction (requete par AJAX) d'ajout de commentaire
		public function addCommentaire()
		{
			$this->load->model('member_model','memdb');
			
			$error = "-";
			if($this->input->post('commentaire',TRUE) == ""){
				$error .= " Missing commentaire -";
			}
			
			//Si on oublie le champ commentaire, on retourne une erreur
			if($error != "-"){
				echo "ERREUR - Commentaire manquant";
			} else {
				echo "Commentaire ajouté";
				$this->memdb->addCommentaire($this->input->post(NULL, TRUE));
			}
		}
		
		//Fonction (requete par AJAX) de modification de statut
		public function updateStatut()
		{
			$this->load->model('member_model','memdb');
			
			$error = "-";
			if($this->input->post('statut',TRUE) == ""){
				$error .= " Missing commentaire -";
			}
			
			//Si on oublie le champ commentaire, on retourne une erreur
			if($error != "-"){
				echo "ERREUR - Statut manquant";
			} else {
				echo "Statut pris en compte";
				$this->memdb->changeStatut($this->input->post(NULL, TRUE));
			}
		}
		
		public function ajaxGetComments()
		{
			$this->load->model('member_model','memdb');
			
			$this->data['comments'] = $this->memdb->getCommentsToId($this->input->get('id', TRUE));
			
			$this->load->view('member/ajax_comments.php', $this->data);
		}
		
		public function ajaxGetStatut()
		{
			$this->load->model('member_model','memdb');
			
			$this->data['statut'] = $this->memdb->getStatutById($this->input->get('id', TRUE));
			
			$this->load->view('member/ajax_statut.php', $this->data);
		}
    }
?>